#!/usr/bin/env python
"""Modulo para leitura de um arquivo de dados"""

from .point import Point
from .segment import Segment

def open_file (name):
	"""Le o arquivo passado, e retorna o seu conteudo
	
	Atualmente, ele espera que o arquivo contenha uma lista de pontos,
	um ponto por linha, as duas coordenadas em cada linha. Exemplo:
	
	0 0
	0 1
	10 100
	
	"""
	f = open (name, 'r')
	#t = range (5000)
	lista = []
	cont = 0
	numFields = -1

	for linha in f.readlines ():
		if linha[0] == '#': continue

		coord = linha.split()

		fields = len (coord)
		if numFields == -1: 
			numFields = fields

		if fields == 0: continue
		if fields != 2 and fields != 4: raise 'numero invalido de coordenadas'
		if fields != numFields: raise 'cada linha deve conter o mesmo numero coordenadas'
		
		if numFields == 2:
			x = float (coord[0])
			y = float (coord[1])
			lista.append (Point (x, y))
		elif numFields == 4:
			x1 = float(coord[0])
			y1 = float(coord[1])
			x2 = float(coord[2])
			y2 = float(coord[3])
			lista.append (Segment(Point(x1, y1), Point(x2, y2)))

	return lista

if __name__ == '__main__':
	import sys

	for i in sys.argv[1:]:
		print((i,':'))
		lista = open_file (i)
		print(('  ',repr(len(lista)), 'pontos:'))
		for p in lista:
			print(p)
