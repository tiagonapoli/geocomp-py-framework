from .common.EventQueue import EventQueue
from .common.CompareMethods import cmpFloat
from .common.GeometricClasses import IntersectionResult, Segment
from .common.BentleyGUI import BentleyGUI

def uniqueList(lst):
        lst.sort()
        i = 0
        ret = []
        while i < len(lst):
            val = lst[i][0]
            aux = set()
            while i < len(lst) and lst[i][0] == val:
                aux.add(lst[i][1])
                aux.add(lst[i][2])
                i += 1
            aux = list(aux)
            aux.sort()
            ret.append(IntersectionResult(val, aux))
        return ret

class BruteForce:
    def __init__(self, segments, segmentsGUI=None):
        self.segs = segments
        for seg in self.segs:
            seg.increasingX()

        self.gui = segmentsGUI != None
        self.BentleyGUI = BentleyGUI(segmentsGUI, self.gui)

    def solve(self):
        ret = []
        for i in range(len(self.segs)):
            self.BentleyGUI.hilight(i, 1)
            self.BentleyGUI.waitsec()
            for j in range(i+1,len(self.segs)):
                self.BentleyGUI.hilight(j, 2)
                self.BentleyGUI.waitsec()
                flag, inter = self.segs[i].intersectSegment(self.segs[j])
                if flag:   
                    ret.append((inter, i,j))
                    self.BentleyGUI.addIntersectionPt(inter)

                self.BentleyGUI.normalize(j)
            self.BentleyGUI.normalize(i)
                
        return uniqueList(ret)
                

def BruteForceRun(l):
    segments = []
    for s in l:
        segments.append(Segment(s.init.x, s.init.y, s.to.x, s.to.y))
    obj = BruteForce(segments,l)
    res = obj.solve()