from .common.EventQueue import EventQueue
from .common.YStructure import YStructure
from .common.GeometricClasses import Segment 
from .common.BentleyGUI import BentleyGUI

class BentleyOttman:
	def __init__(self, segments, verbose, segmentsGUI=None):
		self.segs = segments
		self.verbose = verbose
		self.gui = segmentsGUI != None
		for seg in self.segs:
			seg.increasingX()
		self.BentleyGUI = BentleyGUI(segmentsGUI, self.gui)


	def getLineSweepPoint(self):
		return self.yStructure.getLineSweepPoint()

	def doEvent(self):
		pt, evType, segID, intersectionLowerSegID = self.evQueue.nextEvent()
		self.BentleyGUI.drawSweepLine(pt.x)
		self.BentleyGUI.drawEvent(pt, evType)
		self.evQueue.delMin()
		
		
		if self.verbose:
			print("EventNow", pt, EventQueue.getEventType(evType), "ID:", segID, "LowerIntersectionID:", intersectionLowerSegID)

		if EventQueue.getEventType(evType) == 'open':
			self.BentleyGUI.openSegment(segID)
			
			self.yStructure.openSegment(self.segs[segID], segID)
			prev = self.yStructure.prev(self.segs[segID], segID)
			next = self.yStructure.next(self.segs[segID], segID)
			if prev != None:
				self.BentleyGUI.hilightTwo(prev.key.getId(), segID)
				self.evQueue.addIntersectionEvent(prev.key.getId(), segID, self.getLineSweepPoint(), evType)
				self.BentleyGUI.unhilightTwo(prev.key.getId(), segID)
				
			if next != None:
				self.BentleyGUI.hilightTwo(segID, next.key.getId())
				self.evQueue.addIntersectionEvent(segID, next.key.getId(), self.getLineSweepPoint(), evType)
				self.BentleyGUI.unhilightTwo(segID, next.key.getId())
				

		elif EventQueue.getEventType(evType) == 'close':
			self.BentleyGUI.closeSegment(segID)
			
			prev, next = self.yStructure.closeSegment(self.segs[segID], segID)
			if prev != None and next != None:
				self.BentleyGUI.hilightTwo(prev.key.getId(), next.key.getId())
				self.evQueue.addIntersectionEvent(prev.key.getId(), next.key.getId(), self.getLineSweepPoint(), evType)
				self.BentleyGUI.unhilightTwo(prev.key.getId(), next.key.getId())

		if EventQueue.getEventType(evType) == 'intersect':
			intersections, downSeg, prevDownSeg, upSeg, nextUpSeg = self.yStructure.swapSegments(self.segs[intersectionLowerSegID], intersectionLowerSegID, pt)
			
			self.BentleyGUI.addIntersectionPt(pt)
			self.ret.append(intersections)
			
			self.BentleyGUI.hilightIntersections(intersections.ids)
			if downSeg != None and prevDownSeg != None:
				self.BentleyGUI.hilightTwo(downSeg.key.getId(), prevDownSeg.key.getId())
				self.evQueue.addIntersectionEvent(prevDownSeg.key.getId(), downSeg.key.getId(), self.getLineSweepPoint(), evType)
				self.BentleyGUI.unhilightTwo(downSeg.key.getId(), prevDownSeg.key.getId())
			
			if upSeg != None and nextUpSeg != None:
				self.BentleyGUI.hilightTwo(upSeg.key.getId(), nextUpSeg.key.getId())
				self.evQueue.addIntersectionEvent(upSeg.key.getId(), nextUpSeg.key.getId(), self.getLineSweepPoint(), evType)
				self.BentleyGUI.unhilightTwo(upSeg.key.getId(), nextUpSeg.key.getId())
			

			self.BentleyGUI.unhilightIntersections()
		
		self.BentleyGUI.undrawEvent()

	def lineSweep(self):
		self.ret = []
		self.evQueue = EventQueue(self.segs, self.verbose, self.gui)  	
		self.yStructure = YStructure(self.verbose)
		while self.evQueue.size() > 0:
			self.doEvent() 
			if self.verbose:
				self.evQueue.status()
				self.yStructure.status()
				print("============================================================")
				input()
		self.BentleyGUI.undrawSweepLine()
		self.BentleyGUI.waitsec()
		return self.ret

	def solve(self):
		return self.lineSweep()


def BentleyOttmanRun(l):
	segments = []
	for s in l:
		segments.append(Segment(s.init.x, s.init.y, s.to.x, s.to.y))
	obj = BentleyOttman(segments, False,l)
	res = obj.solve()
	



