from .common.CompareMethods import *
from .common.SegmentsGenerator import Generator
from .common.GeometricClasses import Segment
from .BentleyOttman import BentleyOttman
from .BruteForce import BruteForce

from random import randint
import traceback
import sys

def seg(x1,y1,x2,y2):
    return Segment(x1,y1,x2,y2)

def printSegList(lst):
    for i in lst:
        print(i)

def printSegments(lst):
    for i in lst:
        print("Segment {} {} {} {}".format(i.p1.x, i.p1.y, i.p2.x, i.p2.y))
    
def printHandCaseList(lst):
    print("[", end="")
    for i in lst:
        print("seg({},{},{},{},),".format(i.p1.x, i.p1.y, i.p2.x, i.p2.y), end="")
    print("],")

class BentleyOttmanTester:

    tests = [
        #[seg(0,0,10,10)],
        # [seg(0,0,10,10), seg(10,0,0,10)],
        # [seg(0,0,10,10), seg(10,0,0,10), seg(2,5,4,5), seg(6,5,10,5)],
        # [seg(0,0,10,10), seg(10,0,0,10), seg(2,5,6,8), seg(6,5,10,5)],
        # [seg(0,0,10,10), seg(10,0,0,10), seg(2,5,5,8), seg(6,5,10,5)],
        # [seg(0,0,10,10), seg(10,0,0,10), seg(2,5,6,5), seg(7,5,10,5)], #3 samepoint intersection
        # [seg(1,1,10,10), seg(10,0,0,10), seg(2,0,8,10), seg(7,5,10,5)], #3 samepoint intersection
        # [seg(0,0,2,2), seg(2,2,3,4)], #Endpoint Intersection
        # [seg(0,0,2,2), seg(2,2,3,2)], #Endpoint Intersection   
        # [seg(0,0,2,2), seg(2,2,3,3)], #Endpoint Intersection With same slope
        # [seg(0,0,2,2), seg(2,2,3,2), seg(2,2,3,3), seg(2,2,3,4)], #Endpoint Intersection With same slope
        # [seg(6,1,18,7), seg(7,14,6,17), seg(7,12,2,3), seg(15,5.5,6,12), seg(7,14,18,14), seg(2,3,18,3)],
        # [seg(0,10,1,9,),seg(6,3,17,1,),seg(12,1,4,1,),seg(8.0,1.0,17,16,)],
        # [seg(5,17,19,11,),seg(5,9,15,15,),seg(12.777777777777779,13.666666666666666,10,13,),],
        # [seg(19,8,6,2,),seg(4,15,12,10,),seg(18.0,7.538461538461538,13,15,),seg(18.0,7.538461538461538,10,19,),],
        # [seg(1,7,10,16,),seg(4,17,15,16,),seg(4,11,10,16,),seg(10,16,11,17,),],
        # [seg(6,17,35,30,),seg(14,6,30,7,),seg(7,13,38,4,),seg(22,6,26,18,),seg(24,32,29,13,),seg(9,28,35,30,),seg(30,20,38,4,),seg(22.170212765957444,6.51063829787234,28,39,),],
        # [seg(1,9,38,10,),seg(13,0,39,24,),seg(2,30,13,25,),seg(15,11,29,18,),seg(0,0,23.406032482598608,9.605568445475638,),seg(12.0,25.454545454545453,26,6,),seg(19.406032482598608,9.489507684253589,37,10,),],
        # [seg(6,33,8,12,),seg(27,26,34,4,),seg(26,1,27,17,),seg(0,8,7,37,),seg(15,34,23,1,),seg(6.0,33.0,34,22,),seg(1.0,39.25,22,13,),seg(1.0,34.95652173913044,29,24,),seg(6,37,10.009756097560976,-1649.102439024388,),],
        # [seg(25,12,29,35,),seg(16,13,32,19,),seg(8,19,31,16,),seg(24,36,30,36,),seg(11,25,15,14,),seg(8,13,20,16,),seg(21.813953488372093,33.85541503989136,29,3,),seg(6,11,25.813953488372093,16.680232558139537,),seg(23.813308687615528,141.76563386468132,26,5,),seg(6,26,28.813308687615528,15.264826101284385,),],
        # [seg(1,17,31,7,),seg(7,33,37,39,),seg(4,2,6,26,),seg(20,11,38,17,),seg(0,15,34,36,),seg(3,4,10,38,),seg(22,39,37,0,),seg(1,17,5,20,),seg(5.0,15.666666666666666,34,8,),seg(1,17,19,4,),seg(5,31,5.008733624454148,14.104803493449783,),seg(1,18,30.0,14.333333333333334,),seg(19,4,20,39,),],
        # [seg(29,38,38,39,),seg(16,23,27,9,),seg(34,39,36,12,),seg(13,17,37,4,),seg(33,11,38,10,),seg(4,15,33,24,),seg(16,26,32,14,),seg(1,23,4,20,),seg(17,24,38.0,39.0,),seg(34.0,39.0,36,13,),seg(10,36,36.032653061224494,38.77215921694765,),seg(27,3,34,39,),seg(0,7,32.0,38.333333333333336,),seg(4,3,35.0,25.5,),],
        # [seg(17,33,30,10,),seg(17,5,27,27,),seg(15,28,38,9,),seg(1,38,11,1,),seg(9,1,34,34,),seg(17,11,18,12,),seg(5,9,25,28,),seg(31,36,38,26,),seg(24,27,27.05426356589147,-337.76633444076396,),seg(20.054597701149426,23.544674318431653,34,13,),seg(6,16,24.053288578824976,20.521104822078886,),seg(29,30,38,9,),seg(11,20,24.05426356589147,20.519379844961243,),seg(3.885979255448494,11.25501328841897,14,28,),],
        # [seg(17,22,28,7,),seg(7,3,34,18,),seg(5,16,29,36,),seg(12,12,26,38,),seg(1,38,25,38,),seg(34,11,35,24,),seg(19,13,25,15,),seg(17,38,19,22,),seg(24.00526315789474,12.447368421052632,33,8,),seg(24,19,26.00526315789474,-2477.552631578204,),seg(21.0,16.545454545454547,30,19,),seg(4,1,24.0,19.288770053475936,),seg(12.221774193548388,33.27699765884363,24,6,),seg(24.0,14.666666666666666,34,15,),seg(1,20,22.696428571428573,14.232142857142858,),],
        # [seg(4,31,7,12,),seg(29,2,31,29,),seg(26,9,27,5,),seg(28,17,36,17,),seg(3,39,35,27,),seg(29,36,30,4,),seg(7,2,19,14,),seg(2,1,9,4,),seg(32,33,37,15,),seg(27,5,39,36,),seg(25.748091603053435,1.84429947784615,34,23,),seg(19,18,33.96396396396396,31.149808694236405,),seg(26,9,30,39,),seg(34,23,36,26,),seg(1,19,30.963963963963963,28.513513513513512,),],
        # [seg(12,5,34,14,),seg(23,31,36,12,),seg(8,12,15,1,),seg(1,36,10,3,),seg(4,10,36,1,),seg(14,7,19,23,),seg(8,17,34,25,),seg(9,38,28,4,),seg(23,25,26,7,),seg(7.360655737704917,-1.519125683060111,34,34,),seg(1,36,26.0,10.727272727272727,),seg(6.422145328719722,4.8244802877149,28,23,),seg(6.422145328719722,4.8244802877149,18,28,),seg(8,12,28,1,),seg(5,33,13.414659011499454,-26.603625309943794,),seg(17,26,17.004739336492893,7.04739336492891,),],
        # [seg(22,9,23,5,),seg(31,4,33,36,),seg(27,19,36,19,),seg(21,3,39,31,),seg(6,14,34,3,),seg(7,34,22,39,),seg(3,31,19,36,),seg(18,1,27,5,),seg(19,18,23,32,),seg(30,35,31.0,4.0,),seg(15,9,34.0,3.0625,),seg(13,2,25.8,6.963265306122449,),seg(3,10,34.00769230769231,20.82103502437631,),seg(6,19,34,3,),seg(9.0,34.666666666666664,10,12,),seg(9.980758952431852,12.43613041154463,35,21,),seg(20,31,24.0,3.6666666666666665,),],
        # [seg(19,20,38,26,),seg(19,31,35,12,),seg(13,14,18,21,),seg(7,22,35,1,),seg(4,3,20,0,),seg(11,9,20,34,),seg(6,13,20,14,),seg(13,22,37,35,),seg(22,18,36,16,),seg(4,18,29,11,),seg(9,15,33.0,14.375,),seg(9,19,26.317286652078774,22.310722100656456,),seg(9,19,21,27,),seg(17,23,28.317286652078774,22.16276531343241,),seg(9.627906976744185,7.096738940374192,27,39,),seg(20,15,26.31728665207877,22.310722100656456,),seg(11.834645669291339,160.4930633670794,14,5,),seg(7,26,33.0,14.375,),seg(29.034188034188034,14.329033611242199,38,25,),],
        # [seg(0,24,1,27,),seg(5,2,16,20,),seg(4,10,38,0,),seg(24,5,35,25,),seg(11,19,27,4,),seg(36,6,39,26,),seg(11,29,14,26,),seg(6,11,9,3,),seg(26,8,37,8,),seg(23,13,28,31,),seg(0.0,24.0,1,5,),seg(0.0,24.0,30,28,),seg(9.0,8.545454545454545,14,0,),seg(0,24,3,34,),seg(9.0,8.545454545454545,14,7,),seg(9.0,8.545454545454545,19,6,),seg(9,35,9.01133786848072,8.52607709750567,),seg(5,2,29,0,),seg(30,35,34.0,1.1764705882352942,),seg(1,27,34.0,1.1764705882352942,),],
        # [seg(0,0,10,0), seg(0,10,10,10), seg(2,5,10,10), seg(2,6,8,-1)]
    
    ]

    def __init__(self, numTests, minimumTestcaseSize, maximumTestcaseSize, minimumPointCoordinate, maximumPointCoordinate, verbose, testPrintInterval):
        self.numTests = numTests
        self.minimumTestcaseSize = minimumTestcaseSize
        self.maximumTestcaseSize = maximumTestcaseSize
        self.minimumPointCoordinate = minimumPointCoordinate
        self.maximumPointCoordinate = maximumPointCoordinate
        self.verbose = verbose
        self.testPrintInterval = testPrintInterval

    def checkAnswer(self, ret, ans, testcase):
        if len(ans) != len(ret):
            printHandCaseList(testcase)
            printSegments(testcase)   
            raise Exception("Teste de BentleyOttman falhou - Tamanho das respostas diferente")
        for i in range(len(ans)):
            if not (ans[i].pt == ret[i].pt):
                printHandCaseList(testcase)
                printSegments(testcase)
                raise Exception("Teste de BentleyOttman falhou - Ponto de intersecao diferente")
            if len(ans[i].ids) != len(ret[i].ids):
                printHandCaseList(testcase)
                printSegments(testcase)
                raise Exception("Teste de BentleyOttman falhou - Numero de segmentos na intersecao diferente")
            for j in range(len(ans[i].ids)):
                if cmpInt(ans[i].ids[j], ret[i].ids[j]) != 0:
                    printHandCaseList(testcase)
                    printSegments(testcase)
                    raise Exception("Teste de BentleyOttman falhou - Segmento diferente na intersecao")

    def runTestcase(self, testcase):
        obj = BentleyOttman(testcase, self.verbose)
        ansObj = BruteForce(testcase)
        
        if self.verbose:
            printSegments(testcase)
            printHandCaseList(testcase)
            print("TEST")
            printSegList(testcase)
            print()
            ret = obj.solve()
        else:
            try:
                ret = obj.solve()
            except Exception:
                printHandCaseList(testcase)
                printSegments(testcase)
                traceback.print_exc()
                exit(1)

        ans = ansObj.solve()  
        if self.verbose:
            print("Result: ")
            for x in ret:
                print(x)
            print("Ans: ")
            for x in ans:
                print(x)
            print("\n\n")
        self.checkAnswer(ret, ans, testcase)

    def handTests(self):
        for testcase in BentleyOttmanTester.tests:
            self.runTestcase(testcase)
            
    def randomTests(self):
        generator = Generator(self.minimumPointCoordinate, self.maximumPointCoordinate)
        for i in range(self.numTests):    
            testcase = generator.generate(randint(self.minimumTestcaseSize, self.maximumTestcaseSize))
            if i % self.testPrintInterval == 0:
                print("Test ", i, " Size ", len(testcase))
            self.runTestcase(testcase)

    def test(self):
        self.handTests()
        self.randomTests()
        

def tests():
    numTests = int(sys.argv[1])
    minimumTestcaseSize = int(sys.argv[2])
    maximumTestcaseSize = int(sys.argv[3])
    minimumPointCoordinate = int(sys.argv[4])
    maximumPointCoordinate = int(sys.argv[5])
    verbose = int(sys.argv[6])
    testPrintInterval = int(sys.argv[7])
    tester = BentleyOttmanTester(numTests, minimumTestcaseSize, maximumTestcaseSize, minimumPointCoordinate, maximumPointCoordinate, verbose == 1, testPrintInterval)
    tester.test()

if __name__ == '__main__':
    tests()

    