from geocomp.common import control
from geocomp.common.point import Point
from geocomp.common.segment import Segment
from geocomp.common import control
from geocomp import config
from .EventQueue import EventQueue


class BentleyGUI:
    
    hilightColor = [
        None,
        config.HILIGHT_COLOR1,
        config.HILIGHT_COLOR2,
        config.HILIGHT_COLOR3,
        config.HILIGHT_COLOR4
    ]

    def __init__(self, segs, gui):
        self.segs = segs
        self.inter = []
        self.gui = gui
        
        self.sweepline = None
        self.evPoint = None
        self.intersectionsOnEvent = []
        self.ystructure = {}

    def hilight(self, i, colorNum = 1):
        if self.gui:
            self.segs[i].hilight(BentleyGUI.hilightColor[colorNum],BentleyGUI.hilightColor[colorNum])
            
    def normalize(self,i, arr=None):
        if self.gui:
            self.segs[i].unhilight()
            
    def waitsec(self):
        if self.gui:
            control.sleep()

    def drawSweepLine(self, x):
        if self.gui:
            self.undrawSweepLine()
            self.sweepline = control.plot_vert_line(x, config.SWEEPLINE_COLOR)
            self.waitsec()

    def undrawSweepLine(self):
        if self.gui:
            if self.sweepline != None:
                control.plot_delete(self.sweepline)
                self.sweepline = None
    
    def drawEvent(self, pt, evType):
        if self.gui:
            self.undrawEvent()
            self.evPoint = control.plot_disc (pt.x, pt.y, config.EV_TYPE_COLOR[EventQueue.getEventType(evType)], config.RADIUS_HILIGHT)
            self.waitsec()

    def undrawEvent(self):
        if self.gui:
            if self.evPoint != None:
                control.plot_delete(self.evPoint)
                self.evPoint = None


    def newPoint(self, pt):
        return Point(pt.x, pt.y)

    def hilightIntersections(self, ids, colorNum = 3):
        if self.gui:
            for i in ids:
                seg = Segment(self.newPoint(self.segs[i].init), self.newPoint(self.segs[i].to))
                seg.hilight(BentleyGUI.hilightColor[colorNum],BentleyGUI.hilightColor[colorNum])
                self.intersectionsOnEvent.append(seg)
            self.waitsec()
            
    def unhilightIntersections(self):
        if self.gui:
            for i in self.intersectionsOnEvent:
                i.unhilight()    
            self.intersectionsOnEvent = []

    def openSegment(self, i, colorNum = 4):
        if self.gui:
            seg = Segment(self.newPoint(self.segs[i].init), self.newPoint(self.segs[i].to))
            self.ystructure[i] = seg
            self.ystructure[i].plot(BentleyGUI.hilightColor[colorNum])
            self.waitsec()

    def closeSegment(self, i):
        if self.gui:
            self.ystructure[i].hide()
            del self.ystructure[i]
            self.waitsec()

    def hilightTwo(self, a, b):
        if self.gui:
            self.hilight(a, 1)
            self.hilight(b, 1)
            self.waitsec()
            
    def unhilightTwo(self, a, b):
        if self.gui:
            self.normalize(a)
            self.normalize(b)
            self.waitsec()


    def addIntersectionPt(self, pt):
        if self.gui:
            pt = Point(pt.x, pt.y)
            self.inter.append(Point(pt.x, pt.y))
            pt.plot(config.INTERSECTION_COLOR, config.RADIUS + 1)
            self.waitsec()