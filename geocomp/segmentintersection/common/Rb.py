from random import randint
import sys

class RedBlackTree:

    RED = True
    BLACK = False

    class Node:
        def __init__(self, key, val = None, l = None, r = None):
            self.key = key
            self.val = val
            self.l = l
            self.r = r
            self.color = RedBlackTree.RED
            
        def get(self):
            return (self.key, self.val)

        def _print(self):
            print("{}: {} {}".format(self, self.l, self.r))

        def __str__(self):
            if self.color:
                return "{} : {}".format(self.key, self.val) 
            else:
                return "{} : {}".format(self.key, self.val) 
            

    def __init__(self, cmpFunc = None, cmpClass = None):
        self.sz = 0
        self.lastSwap = [None, None]
        self.root = None
        self.error = [] 
        if cmpFunc != None:
            self.cmpFunc = cmpFunc
            self.cmpClass = None
        else:
            self.cmpFunc = None
            self.cmpClass = cmpClass

    def _cmp(self, a, b):
        if self.cmpFunc != None:
            return self.cmpFunc(a,b)
        else:
            return self.cmpClass.cmp(a,b)

    def _isRed(self, h):
        if h == None or h.color != RedBlackTree.RED:
            return False
        return True

    def _isBlack(self, h):
        return not self._isRed(h)

    def _rotateLeft(self, h):
        x = h.r
        h.r = x.l
        x.l = h
        x.color = h.color
        h.color = RedBlackTree.RED
        return x

    def _rotateRight(self, h):
        x = h.l
        h.l = x.r
        x.r = h
        x.color = h.color
        h.color = RedBlackTree.RED
        return x

    def _flipColors(self, h):
        h.color = not h.color
        h.l.color = not h.l.color
        h.r.color = not h.r.color

    def _moveRedLeft(self, h):
        self._flipColors(h)
        if(self._isRed(h.r.l)):
            h.r = self._rotateRight(h.r)
            h = self._rotateLeft(h)
            self._flipColors(h)
        return h

    def _moveRedRight(self, h):
        self._flipColors(h)
        if self._isRed(h.l.l):
            h = self._rotateRight(h)
            self._flipColors(h)
        return h

    def _fix(self, h):
        if (self._isRed(h.r) and self._isBlack(h.l)): 
            h = self._rotateLeft(h)

        if(self._isRed(h.l) and self._isRed(h.l.l)):
            h = self._rotateRight(h)

        if(self._isRed(h.l) and self._isRed(h.r)):
            self._flipColors(h)
        
        return h

    def _findMinNode(self, h):
        while(h.l != None):
            h = h.l
        return h

    def _deleteMin(self, h):
        if h.l == None:
            return None
        
        if self._isBlack(h.l) and self._isBlack(h.l.l):
            h = self._moveRedLeft(h)
        h.l = self._deleteMin(h.l)
        return self._fix(h)

    def _delete(self, h, key):

        if self._cmp(key, h.key) < 0:
            if self._isBlack(h.l) and self._isBlack(h.l.l):
                h = self._moveRedLeft(h)
            h.l = self._delete(h.l,key)
        else:
            if self._isRed(h.l):
                h = self._rotateRight(h)
            
            if self._cmp(key, h.key) == 0 and h.r == None:                
                return None

            if self._isBlack(h.r) and self._isBlack(h.r.l):
                h = self._moveRedRight(h)

            if self._cmp(key, h.key) == 0:
                aux = self._findMinNode(h.r)
                h.key = aux.key
                h.val = aux.val
                h.r = self._deleteMin(h.r)
            else:
                h.r = self._delete(h.r, key)

        return self._fix(h)
        
  
    def _inorder(self, h):
        if(h == None):
            return []
        #h._print()
        ret = self._inorder(h.l)
        ret += [h.get()]
        ret += self._inorder(h.r)
        return ret

    def _next(self, h, key):
        if h == None:
            return None
        ret = None
        if self._cmp(key, h.key) == 0:
            if h.r != None:
                aux = h.r
                while aux.l != None:
                    aux = aux.l
                ret = aux
        elif self._cmp(key, h.key) < 0:
            ret = self._next(h.l, key)
            if ret == None:
                ret = h
        else:
            ret = self._next(h.r, key)
        return ret

    def _prev(self, h, key):
        if h == None:
            return None
        
        ret = None

        if self._cmp(key, h.key) == 0:
            if h.l != None:
                aux = h.l
                while aux.r != None:
                    aux = aux.r
                ret = aux

        elif self._cmp(key, h.key) < 0:
            ret = self._prev(h.l, key)
        else:
            ret = self._prev(h.r, key)
            if ret == None:
                ret = h
        return ret

    def _insert(self, key, val, h):
        if(h == None):
            #print("Cheguei")
            self.sz += 1
            return RedBlackTree.Node(key, val)

        #print("now", h)
        if self._cmp(key, h.key) == 0:
        #    print("Igual")
            h.val = val
        elif self._cmp(key, h.key) < 0:
        #    print("Esquerda")
            h.l = self._insert(key, val, h.l)
        else:
        #    print("Direita")
            h.r = self._insert(key, val, h.r)

        return self._fix(h)


    def prev(self, key):
        return self._prev(self.root, key)
    
    def next(self, key):
        return self._next(self.root, key)
    

    def undoSwap(self):
        a = self.lastSwap[0]
        b = self.lastSwap[1]
        a.val, b.val = b.val, a.val
        a.key, b.key = b.key, a.key
    
    def swap(self, key1, key2):
        a = self.find(key1)
        b = self.find(key2)
        self.lastSwap = [a,b]
        a.val, b.val = b.val, a.val
        a.key, b.key = b.key, a.key
    
    def inorder(self):
        return self._inorder(self.root)
        
    def find(self, key):
        x = self.root
        while x != None:
            #print("find now", x)
            if self._cmp(key, x.key) < 0:
             #   print("find -> esquerda")
                x = x.l
            elif self._cmp(key, x.key) > 0:
              #  print("find -> direita")
                x = x.r
            else:
               # print("find -> achei")
                return x
        return None

    def insert(self, key, val=None):
        self.root = self._insert(key, val, self.root)
        self.root.color = RedBlackTree.BLACK
        return

    def getMin(self):
        if self.sz == 0:
            return None
        return self._findMinNode(self.root).get()

    def delete(self, key):
        if self.find(key) == None:
            return
        self.root = self._delete(self.root, key)
        self.sz -= 1
        if self.root != None:
            self.root.color = RedBlackTree.BLACK

    def size(self):
        return self.sz

    def lowerBound(self, key):
        x = self.root
        res = None
        prev = None
        while x != None:
            if self._cmp(key, x.key) <= 0:
                res = x
                x = x.l
            elif self._cmp(key, x.key) > 0:
                prev = x
                x = x.r
        if res != None and res.l != None:
            prev = res.l
            while prev.r != None:
                prev = prev.r
        return (prev, res)


    def _checkValidColor(self, vtx):
        if self._isBlack(vtx):
            if self._isRed(vtx.r):
                self.error.append("Vertice vermelho a direita: {} {}".format(vtx.debug(), vtx.r.debug()))
                return False
        else:
            if self._isRed(vtx.l):
                self.error.append("Vertice vermelho seguido de vermelho {} {}".format(vtx.debug(), vtx.l.debug()))
                return False
            if self._isRed(vtx.r):
                self.error.append("Vertice vermelho seguido de vermelho e a direita {} {}".format(vtx.debug(), vtx.r.debug()))
                return False
        return True

    def _dfs(self, vtx, h):
        if vtx == None:
            return h + 1
        
        if self._checkValidColor(vtx) == False:
            return False

        if self._isBlack(vtx):
            sum = 1
        else:
            sum = 0

        ret1 = self._dfs(vtx.l, h + sum) 
        if ret1 == False:
            return False
        ret2 = self._dfs(vtx.r, h + sum)
        if ret1 != ret2:
            self.error.append("Vertices com alturas diferentes enraizados em {}: l={} r={}".format(vtx.debug(), ret1, ret2))
            return False
                
        if ret2 == False or ret1 != ret2:
            return False
        return ret1

    def checkIfIsRB(self):
        return self._dfs(self.root, 0) != False


def cmpFunc(a,b):
    if a < b:
        return -1
    elif a > b:
        return 1
    else:
        return 0

class cmpClass:    
    def cmp (self,a,b):
        if a < b:
            return -1
        elif a > b:
            return 1
        else:
            return 0


def _check(rb, ans):
    if rb.checkIfIsRB() == False:
        print(rb.error)
        return False
    ordenado = rb.inorder()
    #print(len(ans), "Lista: ", ans)
    #print(len(ordenado), "Inorder: ", ordenado, "\n")
    if len(ordenado) != len(ans):
        return False
    for i in range(len(ordenado)):
        if ordenado[i][0] != ans[i]:
            return False
    return True


def _addToAns(ans, key):
    lo = 0
    hi = len(ans)
    mid = -1
    while lo < hi:
        mid = (lo + hi)//2
        if ans[mid] >= key:
            hi = mid
        else:
            lo = mid+1
    if hi < len(ans) and ans[hi] == key:
        return    
    else:
        ans.insert(hi,key)

def _testInsert(rb1, rb2, ans, mini, maxi):
    x = randint(mini, maxi)
    #print("Insert ", x)
    _addToAns(ans, x)
    rb1.insert(x)
    rb2.insert(x)


def _testDelete(rb1, rb2, ans):
    if len(ans) == 0:
        return
    x = randint(0, len(ans) - 1)
    #print("Delete ", ans[x])
    rb1.delete(ans[x])
    rb2.delete(ans[x])
    ans.pop(x)
    #print(ans)


def _testPrevAndNext(rb, ans):
    if len(ans) == 0:
        return
    x = randint(0, len(ans) - 1)
    #print("PrevAndNext ", ans[x])
    prev = rb.prev(ans[x])
    next = rb.next(ans[x])
    #print(prev,ans[x],next)
    #print(ans)
    if x-1 < 0 and prev != None:
        raise Exception("Deu ruim no prev, era pra ser None")
    if x-1 >= 0 and prev.key != ans[x-1]: 
        raise Exception("Deu ruim no prev {}, deu diferente {}".format(prev, ans[x-1]))
    if x+1 >= len(ans) and next != None:
        raise Exception("Deu ruim no next, era pra ser None")
    if x+1 < len(ans) and next.key != ans[x+1]:
        raise Exception("Deu ruim no next {}, deu diferente {}".format(next, ans[x+1]))              

def _testMinimum(rb, ans):
    if len(ans) == 0:
        return
    mini = rb.getMin()
    #print("Minimum ", mini, ans[0])
    if ans[0] != mini[0]:
        raise Exception("Deu ruim no minimo. Resposta: {}. Conseguido: {}".format(ans[0], mini))

def _testSwap(rb, ans):
    if len(ans) == 0:
        return
    x = randint(0, len(ans) - 1)
    y = randint(0, len(ans) - 1)
    #print("Swap ", ans[x], ans[y])
    ans[x], ans[y] = ans[y], ans[x]
    rb.swap(ans[x], ans[y])
    if(_check(rb, ans) == False):
        raise Exception("Deu ruim na rb swap")
    ans[x], ans[y] = ans[y], ans[x]
    rb.undoSwap()
    if(_check(rb, ans) == False):
        raise Exception("Deu ruim na rb undo swap")

def _testSize(rb, ans):
    if rb.size() != len(ans):
        raise Exception("Deu ruim no size")

def _lowerBoundAns(ans, x):
    lo = 0
    hi = len(ans)
    mid = -1
    while lo < hi:
        mid = (lo + hi)//2
        if ans[mid] >= x:
            hi = mid
        else:
            lo = mid+1
    ret = None
    prev = None
    if hi < len(ans):
        ret = ans[hi]
    if hi - 1 >= 0:
        prev = ans[hi-1]
    return (prev,ret)

def _cmpLowerBound(node, a):
    if a == None and node == None:
        return True
    if a == None and node != None:
        return False
    elif a != None and node == None:
        return False
    elif a == node.key:
        return True
    return False

def _testLowerBound(rb, ans):
    x = randint(-1000, 1000)
    res = _lowerBoundAns(ans, x)
    ret = rb.lowerBound(x)

    if _cmpLowerBound(ret[0],res[0]) == False or _cmpLowerBound(ret[1], res[1]) == False:
       raise Exception("Deu ruim no lower bound")


def test():
    mini = -1000
    maxi = 1000
    n = int(sys.argv[1])
    print("RB - {} testes - {} -> {}".format(n,mini,maxi))

    ans = []

    cmpObj = cmpClass()
    rb1 = RedBlackTree(cmpFunc=cmpFunc)
    rb2 = RedBlackTree(cmpClass=cmpObj)

    _testMinimum(rb1,ans)
    _testPrevAndNext(rb1,ans)
    _testSwap(rb1,ans)
    _testSize(rb1,ans)
    _testLowerBound(rb1,ans)

    _testMinimum(rb2,ans)
    _testPrevAndNext(rb2,ans)
    _testSwap(rb2,ans)
    _testSize(rb2,ans)
    _testLowerBound(rb2,ans)


    for i in range(n):

        tipo = randint(0,2)
        if(tipo in [0,1]):
            _testInsert(rb1,rb2,ans,mini,maxi)
        elif(tipo in [2]):
            _testDelete(rb1,rb2,ans)

        _testMinimum(rb1,ans)
        _testPrevAndNext(rb1,ans)
        _testSwap(rb1,ans)
        _testSize(rb1,ans)
        _testLowerBound(rb1,ans)

        _testMinimum(rb2,ans)
        _testPrevAndNext(rb2,ans)
        _testSwap(rb2,ans)
        _testSize(rb2,ans)
        _testLowerBound(rb2,ans)

        if(i % 4000 == 0):
            print("RB size", rb1.size())
        
        if(i % 50 == 0 and _check(rb1, ans) == False):
            raise Exception("Deu ruim na RB")
        if(i % 50 == 0 and _check(rb2, ans) == False):
            raise Exception("Deu ruim na RB")

if __name__ == '__main__':
    test()


    




