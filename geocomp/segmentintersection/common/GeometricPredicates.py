import sys
from . import Constants
from random import randint
from . import GeometricClasses
from .CompareMethods import *
import geocomp.common.prim as prim

def cross(o, a, b):
    oa = a - o
    ob = b - o
    return oa.x * ob.y - oa.y * ob.x

def cross2(a, b):
    prim.num_area2 += 1
    return a.x * b.y - a.y * b.x
    
def collinear(a, b, x):
    return abs(cross(a,b,x)) <  Constants.EPS

def dot(a, b):
    return a.x * b.x + a.y * b.y
    
def left(o, a, b):
    return cross(o, a, b) >= -Constants.EPS
    
def leftStrict(o, a , b):
    return cross(o, a, b) > Constants.EPS

def between(a, b, p):
    if(collinear(a,b,p) and 
       cmpFloat(min([a.x,b.x]), p.x) <= 0 and cmpFloat(p.x, max([a.x,b.x])) <= 0 and
       cmpFloat(min([a.y,b.y]), p.y) <= 0 and cmpFloat(p.y, max([a.y,b.y])) <= 0):
        return True
    return False

def betweenStrict(a, b, p):

    if(collinear(a,b,p) and
       ((cmpFloat(a.x,b.x) != 0 and cmpFloat(min([a.x,b.x]), p.x) < 0 and cmpFloat(p.x, max([a.x,b.x])) < 0) or
        (cmpFloat(a.y,b.y) != 0 and cmpFloat(min([a.y,b.y]), p.y) < 0 and cmpFloat(p.y, max([a.y,b.y])) < 0))):
        return True
    return False

def intersectionPoint(a,b,c,d):
    # a + ab * x = c + cd * y
    # (a + ab * x) ^ ab = (c + cd * y) ^ ab
    # a ^ ab = c ^ ab + (cd ^ ab) * y
    # (ca ^ ab) / (cd ^ ab)=  y 
    ab = b - a
    cd = d - c
    ca = a - c
    if cross2(cd,ab) == 0:
        if cross2(ca,ab) != 0:
            return None
        return a
    y = cross2(ca,ab) / cross2(cd,ab)
    return c + cd * y


def intersectProp(a1,a2,b1,b2):
    if collinear(a1,a2,b1) or collinear(a1,a2,b2) or collinear(b1,b2,a1) or collinear(b1,b2,a2):
        return False
    return (leftStrict(a1,a2,b1) != leftStrict(a1,a2,b2)) and (leftStrict(b1,b2,a1) != leftStrict(b1,b2,a2))

def intersectSegment(a1, a2, b1, b2):
    #Returns pair (True/False, Intersection point)
    ret = (False, intersectionPoint(a1,a2,b1,b2))

    if intersectProp(a1,a2,b1,b2):
        return (True, intersectionPoint(a1,a2,b1,b2))
    
    if between(a1,a2,b1):
        ret = (True, b1)
    elif between(a1,a2,b2):
        ret = (True, b2)
    elif between(b1,b2,a1):
        ret = (True, a1)
    elif between(b1,b2,a2):
        ret = (True, a2)

    return ret

def segmentOverlap(a,b,c,d):
    if (a == c and b == d) or (a == d and b == c):
       return True
    
    ab = b-a
    cd = d-c
    if cmpFloat(cross2(ab,cd), 0) != 0:
        return False
    elif betweenStrict(a,b,c) or betweenStrict(a,b,d) or betweenStrict(c,d,a) or betweenStrict(c,d,b):
        return True
    return False


def intersectCircle():
    #Returns pair (True/False, [Intersections point])
    pass


def gera(pts):
    a = randint(0,len(pts)-1)
    aa = randint(0,len(pts)-1)
    while(a == aa):
        aa = randint(0,len(pts)-1)
    a = pts[a]
    aa = pts[aa]
    return (a,aa)

def intersectionPointTest(pts, tests):
    while tests > 0:
        tests -= 1
        if tests % 500000 == 0:
            print(tests)
        a,aa = gera(pts)        
        b,bb = gera(pts)
        inter = intersectionPoint(a,aa,b,bb)
        if inter == None and cross2(a-aa,b-bb) != 0:
            print("Deu none, nao era", a,aa,b,bb, inter)
            return 127
        elif inter != None:
            if not (abs(cross(a,aa,inter)) < Constants.EPS and abs(cross(b,bb,inter)) < Constants.EPS):
                print("Deu ruim, ponto invalido", a, aa, b, bb, inter)
                print("cross(Point({},{}), Point({},{}), Point({},{}))".format(a.x,a.y,aa.x,aa.y,inter.x,inter.y))
                print("cross(Point({},{}), Point({},{}), Point({},{}))".format(b.x,b.y,bb.x,bb.y,inter.x,inter.y))
                return 127

def intersection_segmentTest(pts, tests):
    while tests > 0:
        tests -= 1
        if tests % 500000 == 0:
            print(tests)
        a,aa = gera(pts)        
        b,bb = gera(pts)
        flag, inter = intersectSegment(a,aa,b,bb)
        if inter == None and cross2(a-aa,b-bb) != 0:
            print("Deu none, nao era", a,aa,b,bb)
            return 127
        elif inter != None:
            if between(a,aa,inter) and between(b,bb,inter):
                if(flag == False):
                    print("Deu ruim no between", a, aa, b, bb, inter)
                    return 127
            else:
                if(flag == True):
                    print("Deu ruim nao era pra dar true", a, aa, b, bb, inter)
                    print(between(a,aa,inter))
                    print(between(b,bb,inter))
                    print("between(Point({},{}),Point({},{}),Point({},{}))".format(a.x,a.y,aa.x,aa.y,inter.x,inter.y))
                    print("between(Point({},{}),Point({},{}),Point({},{}))".format(b.x,b.y,bb.x,bb.y,inter.x,inter.y))
                    return 127


def test():

    tipo = int(sys.argv[1])
    a = 0
    b = 500
    pts = []
    for i in range(a,b):
        for j in range(a,b):
            pts.append(GeometricClasses.Point(i,j))

    tests = int(sys.argv[2])
    if tipo == 0:
        print("INTER PT TEST", tests, "{}->{}".format(a,b))
        return intersectionPointTest(pts, tests)
    elif tipo == 1:
        print("INTER SEGM TEST", tests, "{}->{}".format(a,b))
        return intersection_segmentTest(pts, tests)


if __name__ == '__main__':
    exit(test())



