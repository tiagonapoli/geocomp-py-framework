from .Rb import RedBlackTree
from .CompareMethods import cmpFloat, cmpPointLexicograph, cmpInt
from .EventQueueGUI import EventQueueGUI

class Event:
    
    def __init__(self, pt, evType, id):
        self.pt = pt
        self.evType = evType
        self.id = id
    
    def __str__(self):
        return str(self.pt) + " " + EventQueue.getEventType(self.evType) + " ID" + str(self.id)

class EventQueue:

    DONT_CARE = -1 
    evType = {
        'open': 0,
        'close': 2,
        'intersect': 1
    }

    @staticmethod
    def getEventType(evNum):
        for a,b in EventQueue.evType.items():
            if b == evNum:
                return a 
    
    @staticmethod
    def __cmpEventQueue(a,b):
        if cmpPointLexicograph(a.pt,b.pt) == 0:
            if a.evType == b.evType:
               return cmpInt(a.id,b.id) 
            else:
               return cmpInt(a.evType, b.evType)
        else:
            return cmpPointLexicograph(a.pt,b.pt)

    @staticmethod
    def queueNodeToString(node):
        return str(node[0]) + " : " + str(node[1])
        
    def __init__(self, segs, verbose, gui):
        self.queue = RedBlackTree(cmpFunc=EventQueue.__cmpEventQueue)
        self.segs = segs
        self.verbose = verbose
        self.EventQueueGUI = EventQueueGUI(gui)
        for i in range(len(segs)):
            self.addOpenEvent(i)
            self.addCloseEvent(i)

    def status(self):
        print("EventQueue Status")
        print([EventQueue.queueNodeToString(x) for x in self.queue.inorder()])
        print()

    def addOpenEvent(self, idx):
        if self.verbose:
            print("Add Open Event: ", Event(self.segs[idx].p1, EventQueue.evType['open'], idx))
        self.EventQueueGUI.addEvent(self.segs[idx].p1, 'open')
        self.queue.insert(Event(self.segs[idx].p1, EventQueue.evType['open'], idx))

    def addCloseEvent(self, idx):
        if(self.verbose):
            print("[EVQUEUE] Add Close Event: ", Event(self.segs[idx].p2, EventQueue.evType['close'], idx))
        self.EventQueueGUI.addEvent(self.segs[idx].p2, 'close')
        self.queue.insert(Event(self.segs[idx].p2, EventQueue.evType['close'], idx))

    def addIntersectionEvent(self, idDown, idUp, lineSweepPoint, evTypeNow):
        flag, inter = self.segs[idDown].intersectSegment(self.segs[idUp])
        if(self.verbose):
            print("[EVQUEUE] Add Intersection Event?", "\n[EVQUEUE] idDown: ", idDown, "\n[EVQUEUE] IdUp: ", idUp, "\n[EVQUEUE] LineSweepPoint: ", lineSweepPoint)  
        if flag and (inter.after(lineSweepPoint)
                or  (inter == lineSweepPoint and evTypeNow != EventQueue.evType['close'])):
            self.EventQueueGUI.addEvent(inter, 'intersect')
            node = self.queue.find(Event(inter, EventQueue.evType['intersect'], EventQueue.DONT_CARE))    
            if(self.verbose):
                print("[EVQUEUE] Added IntersectionPoint: ", inter)
                print("[EVQUEUE] Intersect node: ", node)
            
            if node == None:
                self.queue.insert(Event(inter, EventQueue.evType['intersect'], EventQueue.DONT_CARE), idDown)
            elif node != None:
                aux = self.segs[idDown].cmpAngularCoef(self.segs[node.val])
                if aux == 0 and cmpInt(idDown, node.val) > 0:
                    node.val = idDown
                elif aux > 0:
                    node.val = idDown

        elif self.verbose:
            print("[EVQUEUE] Not Added")            

    def nextEvent(self):
        ret = self.queue.getMin()
        self.queue.delete(ret[0])
        self.EventQueueGUI.addToDelEventQueue(ret[0].pt, EventQueue.getEventType(ret[0].evType))
        return ret[0].pt, ret[0].evType, ret[0].id, ret[1]
            
    def delMin(self):
        self.EventQueueGUI.delEventOnQueue()

    def size(self):
        return self.queue.size()

