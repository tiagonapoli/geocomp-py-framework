from geocomp.common import control
from geocomp.common.point import Point
from geocomp.common.segment import Segment
from geocomp.common import control
from geocomp import config
from .CompareMethods import cmpFloat


class EventQueueGUI:

    def __init__(self, gui):
        self.gui = gui
        self.nextDel = None
        self.evQueue = []

    def waitsec(self):
        if self.gui:
            control.sleep()

    def addEvent(self, pt, evType):
        if self.gui:
            pt1 = Point(pt.x, pt.y)
            pt1.plot(config.EV_TYPE_COLOR[evType])
            self.evQueue.append((pt, pt1, evType))
            if evType == 'intersect':
                self.waitsec()
          
    def delEventOnQueue(self):
        if self.gui:
            delete = []
            for i in range(len(self.evQueue)):
                if self.nextDel[0] == self.evQueue[i][0] and self.nextDel[1] == self.evQueue[i][2]:
                    delete.append(i)
            
            for i in delete[::-1]:
                self.evQueue[i][1].unplot()
                self.evQueue.pop(i)


    def addToDelEventQueue(self, pt, evType):
        if self.gui:
            self.nextDel = (pt, evType)

                

