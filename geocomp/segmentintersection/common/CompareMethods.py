from . import Constants

def cmpFloat(a, b):
    if abs(a-b) < Constants.EPS:
        return 0
    elif a < b - Constants.EPS:
        return -1
    else:
        return 1

def cmpPointLexicograph(a,b):
    if a == b:
        return 0
    elif cmpFloat(a.x, b.x) < 0 or (cmpFloat(a.x, b.x) == 0 and cmpFloat(a.y, b.y) < 0):
        return -1
    else:
        return 1

def cmpInt(a, b):
    if a == b:
        return 0
    elif a < b:
        return -1
    else:
        return 1
    
