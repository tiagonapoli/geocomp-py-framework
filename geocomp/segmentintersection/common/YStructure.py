from .Rb import RedBlackTree
from .GeometricPredicates import leftStrict, collinear
from .GeometricClasses import Point, IntersectionResult
from .CompareMethods import cmpInt
from .EventQueue import EventQueue
from . import Constants

class YStructureSegment:
    
    def __init__(self, seg, id):
        self.seg = seg
        self.id = id
    
    def getSeg(self):
        return self.seg

    def getId(self):
        return self.id

    def angularCoef(self):
        return self.seg.angularCoef()

    def cmpAngularCoef(self, b):
        return self.seg.cmpAngularCoef(b.getSeg())
            
    def intersectSegment(self, b):
        return self.seg.intersectSegment(b.getSeg())

    def isPointCollinear(self, pt):
        return self.seg.isPointCollinear(pt)

    def isPointLeftStrict(self, pt):
        return self.seg.isPointLeftStrict(pt)

    def isPointLeft(self, pt):
        return self.seg.isPointLeft(pt)

    def __str__(self):
        return "ID" + str(self.id) + " " + str(self.seg)

class YStructureMapValue:

    def __init__(self, node, pt):
        self.node = node
        self.pt = pt

    def __str__(self):
        return "ID" + str(self.node.key.getId()) + " " + str(self.pt)  
    

class cmpYStructure:
    
    def __init__(self):
        self.lineSweepPoint = Point(-Constants.INF, -Constants.INF)
        self.evType = None

    def cmp(self, keyFocused, keyB):
        if keyB.isPointCollinear(self.lineSweepPoint):
            aux = keyFocused.cmpAngularCoef(keyB)
            if aux == 0:
                aux = cmpInt(keyFocused.id, keyB.id)
            if self.evType == EventQueue.evType['close']:
                return aux
            else:
                return -1 * aux
        else:
            if keyB.isPointLeftStrict(self.lineSweepPoint): 
                return 1
            else:
                return -1


class YStructure:

    @staticmethod
    def mapNodeToString(node):
        return str(node[0]) + " : " + str(node[1])
 
    def __init__(self, verbose):
        self.cmpClass = cmpYStructure()
        self.map = RedBlackTree(cmpClass=self.cmpClass)
        self.verbose = verbose

    def status(self):
        print("YStructure Status")
        print("LineSweepPoint: ", self.getLineSweepPoint())
        print([YStructure.mapNodeToString(x) for x in self.map.inorder()])
        print()
        

    def getLineSweepPoint(self):
        return self.cmpClass.lineSweepPoint

    def updateSweepLinePoint(self, pt, evType):
        self.cmpClass.lineSweepPoint = pt
        self.cmpClass.evType = evType

    def prev(self, seg, idx):
        return self.map.prev(YStructureSegment(seg, idx))

    def next(self, seg, idx):
        return self.map.next(YStructureSegment(seg, idx))

    def updateUpIntersection(self, node, upNode):
        segX = node.key
        segUp = upNode.key
        flag, inter = segX.intersectSegment(segUp)
        if flag:
            node.val = YStructureMapValue(upNode, inter)
            if self.verbose:
                print("[YSTRUCT] Intersection with up node exists. Update")
                print("[YSTRUCT] NodeNow: ", node)
                print("[YSTRUCT] UpNode: ", upNode) 
        else:
            node.val = None


    def openSegment(self, seg, idx):
        self.updateSweepLinePoint(seg.p1, EventQueue.evType['open'])

        if self.verbose:
            print("[YSTRUCT] Open segment", idx, seg)

        self.map.insert(YStructureSegment(seg, idx))
        node = self.map.find(YStructureSegment(seg, idx))

        next = self.next(seg, idx)
        if next != None:
            self.updateUpIntersection(node, next)

        prev = self.prev(seg, idx)
        if prev != None:
            self.updateUpIntersection(prev, node)

        if self.verbose:
            self.status()
            print("[YSTRUCT] Next segment", next)
            print("[YSTRUCT] Prev segment", prev)
            print()



    def closeSegment(self, seg, idx):
        self.updateSweepLinePoint(seg.p2, EventQueue.evType['close'])
        if self.verbose:
            print("[YSTRUCT] Close segment", idx, seg)

        self.map.delete(YStructureSegment(seg, idx))
        prev,next = self.map.lowerBound(YStructureSegment(seg, idx))
        if self.verbose:
            print("[YSTRUCT] ", prev)
            print("[YSTRUCT] ", next)
            self.status()
            
        if prev != None and next != None:
            self.updateUpIntersection(prev, next)
        elif prev != None:
            prev.val = None

        if self.verbose:
            self.status()
            print("[YSTRUCT] Next segment", next)
            print("[YSTRUCT] Prev segment", prev)
            print()

        return prev,next


    def swapSegments(self, seg, idx, intersectionPoint):
        self.updateSweepLinePoint(intersectionPoint, EventQueue.evType['intersect'])
    
        lst = []
        node = self.map.find(YStructureSegment(seg, idx))
        lst.append(node)

        if self.verbose:
            print("[YSTRUCT] Swap Segments")
            print("[YSTRUCT] Lower node of intersection: ", node)

        while node.val != None and node.val.pt ==  intersectionPoint:
            node = node.val.node
            lst.append(node)

        prev = self.prev(lst[0].key.getSeg(), lst[0].key.getId())
        next = self.next(lst[-1].key.getSeg(), lst[-1].key.getId())
        
        if self.verbose:
            print("[YSTRUCT] Next segment", next)
            print("[YSTRUCT] Prev segment", prev)

        ret = []
        for i in lst:
            ret.append(i.key.getId())

        for i in range(len(lst)//2):
            lst[i].key, lst[len(lst)-1-i].key = lst[len(lst)-1-i].key, lst[i].key
        
        lst[len(lst) - 1].val = None
        for i in range(len(lst)-1):
            lst[i].val = YStructureMapValue(lst[i+1], intersectionPoint)

        if prev != None:
            self.updateUpIntersection(prev, lst[0])
        
        if next != None:
            self.updateUpIntersection(lst[-1], next)

        if self.verbose:
            print("[YSTRUCT] ", [YStructure.mapNodeToString(x.get()) for x in lst])
            self.status()
            
        ret.sort()
        return IntersectionResult(intersectionPoint, ret), lst[0], prev, lst[-1], next
        
        
        

        
    
    
    
        