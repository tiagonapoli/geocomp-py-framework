from .GeometricClasses import Point, Segment
from .GeometricPredicates import intersectionPoint
from random import randint

class Generator:

    def __init__(self,a,b):
        self.a = a
        self.b = b
        self.pt = []
        for x1 in range(a,b):
            for y1 in range(a,b):
                self.pt.append(Point(x1,y1))


    def check(self, seg):
        if seg.p1 == seg.p2 or seg.isVertical():
            return False

        if abs(seg.p1.x) > abs(max(self.a,self.b)) * 10 or abs(seg.p1.y) > abs(max(self.a,self.b)) * 10:
            return False
        if abs(seg.p2.x) > abs(max(self.a,self.b)) * 10 or abs(seg.p2.y) > abs(max(self.a,self.b)) * 10:
            return False

        for i in self.segsFloat:
            if i.overlap(seg):
                return False

        for i in self.segsInt:
            if i.overlap(seg):
                return False
        return True
        

    def generateEndpointIntersection(self):
        if len(self.endpoints) == 0:
            return None
        ret = None
        cnt = 3
        while ret == None and cnt > 0:
            a = self.endpoints[randint(0, len(self.endpoints)-1)]
            b = self.pt[randint(0, len(self.pt)-1)]
            cnt -= 1
            ret = Segment(a.x,a.y, b.x, b.y)
            if self.check(ret) == False:
                ret = None
        return ret

    def generateMultipleIntersection(self):
        if len(self.intersections) == 0:
            return None
        ret = None
        cnt = 3
        while ret == None and cnt > 0:
            inter = self.intersections[randint(0, len(self.intersections)-1)]
            a = self.pt[randint(0, len(self.pt)-1)]
            if inter.x > a.x:
                x = inter.x + randint(2,5)
            else:
                x = inter.x - randint(2,5)
            b = intersectionPoint(a,inter, Point(x,0), Point(x,1))
            cnt -= 1
            if a == None or b == None:
                ret = None
                continue
            
            ret = Segment(a.x,a.y, b.x, b.y)
            if self.check(ret) == False:
                ret = None
        return ret

    def generateEndpointInIntersection(self):
        if len(self.intersections) == 0:
            return None
        ret = None
        cnt = 3
        while ret == None and cnt > 0:
            a = self.intersections[randint(0, len(self.intersections)-1)]
            b = self.pt[randint(0, len(self.pt)-1)]
            cnt -= 1
            ret = Segment(a.x,a.y, b.x, b.y)
            if self.check(ret) == False:
                ret = None
        return ret
    
    def generateEndpointInsideSeg(self):
        segs = self.segsInt
        if len(segs) == 0:
            return None
        ret = None
        cnt = 3
        while ret == None or cnt > 0:
            id1 = randint(0, len(segs)-1)
            x = randint(min(segs[id1].p1.x, segs[id1].p2.x), max(segs[id1].p1.x, segs[id1].p2.x))
            a = intersectionPoint(segs[id1].p1,segs[id1].p2,Point(x,0),Point(x,1))
            id2 = randint(0, len(self.pt)-1)
            ret = Segment(a.x,a.y, self.pt[id2].x, self.pt[id2].y)
            cnt -= 1
            if self.check(ret) == False:
                ret = None
        return ret
        

    def addExistentIntersections(self, seg):
        for i in self.segsInt:
            flag, inter = seg.intersectSegment(i)
            if flag:
                self.intersections.append(inter)

    def generate(self, n):
        self.endpoints = []
        self.intersections = []
        self.segsInt = []
        self.segsFloat = []

        while len(self.segsInt) + len(self.segsFloat) < n:
            id1 = randint(0, len(self.pt)-1)
            id2 = randint(0, len(self.pt)-1)
            seg = Segment(self.pt[id1].x, self.pt[id1].y, self.pt[id2].x, self.pt[id2].y)
            while self.check(seg) == False:
                id1 = randint(0, len(self.pt)-1)
                id2 = randint(0, len(self.pt)-1)
                seg = Segment(self.pt[id1].x, self.pt[id1].y, self.pt[id2].x, self.pt[id2].y)        
            self.addExistentIntersections(seg)
            self.segsInt.append(seg)
            self.endpoints.append(seg.p1)
            self.endpoints.append(seg.p2)

            if len(self.segsInt) + len(self.segsFloat) == n:
                continue

            segType = randint(0,3)
            seg = None
            
            if segType in [0]:
                seg = self.generateEndpointInsideSeg()
                #print("endpoint inside seg", seg)
            elif segType in [1]:
                seg = self.generateEndpointIntersection()
                #print("endpoint intersection", seg)
            elif segType in [2]:
                seg = self.generateMultipleIntersection()
                #print("multiple intersection", seg)
            elif segType in [3]:
                seg = self.generateEndpointInIntersection()
                #print("endpoint in intersection", seg)
            if seg != None:
                self.addExistentIntersections(seg)
                self.segsFloat.append(seg)
                self.endpoints.append(seg.p1)
                self.endpoints.append(seg.p2)

        #print()
        return self.segsInt + self.segsFloat

