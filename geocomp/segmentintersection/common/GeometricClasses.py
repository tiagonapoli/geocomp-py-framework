from . import Constants
from . import GeometricPredicates 
from .CompareMethods import *

class Point:
    
    def __init__(self, x,y):
        self.x = x
        self.y = y

    def __sub__(self, b):
        return Point(self.x-b.x, self.y-b.y)

    def __add__(self, b):
        return Point(self.x + b.x, self.y + b.y)
    
    def __mul__(self, x):
        return Point(self.x * x, self.y * x)

    def __eq__(self, b):
        if isinstance(b, Point):
            return cmpFloat(self.x, b.x) == 0 and cmpFloat(self.y, b.y) == 0
        return False

    def __str__(self):
        return "({},{})".format(self.x, self.y)

    def after(self, b):
        if cmpFloat(self.x, b.x) == 0:
            return cmpFloat(self.y, b.y) > 0
        else:
            return cmpFloat(self.x, b.x) > 0

    def __lt__(self, b):
        if cmpFloat(self.x, b.x) == 0:
            return cmpFloat(self.y, b.y) < 0
        else:
            return cmpFloat(self.x, b.x) < 0

class Segment:

    def __init__(self, x1, y1, x2, y2):
        self.p1 = Point(x1,y1)
        self.p2 = Point(x2,y2)
        self.tg = (self.p1.y - self.p2.y, self.p1.x - self.p2.x)
        if self.tg[1] < 0:
            self.tg = (-self.tg[0], -self.tg[1])

    def angularCoef(self):
        return self.tg

    def cmpAngularCoef(self, b):
        tgA = self.tg
        tgB = b.tg
        if tgA[0] * tgB[1] < tgB[0] * tgA[1]:
            return -1
        elif tgA[0] * tgB[1] > tgB[0] * tgA[1]:
            return 1
        return 0

    def isVertical(self):
        return cmpFloat(self.p1.x, self.p2.x) == 0

    def increasingX(self):
        if self.p1.x == self.p2.x and self.p1.y > self.p2.y:
            self.p1, self.p2 = self.p2, self.p1
        elif self.p1.x > self.p2.x:
            self.p1, self.p2 = self.p2, self.p1
            
    def intersectSegment(self, b):
        return GeometricPredicates.intersectSegment(self.p1, self.p2, b.p1, b.p2)

    def isPointCollinear(self, pt):
        return GeometricPredicates.collinear(self.p1, self.p2, pt)

    def isPointLeftStrict(self, pt):
        return GeometricPredicates.leftStrict(self.p1, self.p2, pt)

    def isPointLeft(self, pt):
        return GeometricPredicates.left(self.p1, self.p2, pt)

    def overlap(self, b):
        return GeometricPredicates.segmentOverlap(self.p1, self.p2, b.p1, b.p2)

    def __str__(self):
        return str(self.p1) + "->" + str(self.p2)

class IntersectionResult:
	
	def __init__(self, pt, ids):
		self.pt = pt
		self.ids = ids

	def __str__(self):
		return str(self.pt) + ": " + str(self.ids)
