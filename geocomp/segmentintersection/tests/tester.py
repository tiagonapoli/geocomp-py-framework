#!/usr/bin/python3
import json
import subprocess

def main():

    command = "./tester "
    with open("tester.json", "r+") as f:
        data = json.load(f)

    for key in data.keys():
        if(key == 'Threads'):
            command += str(data[key]) + " "
            continue
        for val in data[key].values():
            command += str(val) + " "
    print(command, "\n")
    process = subprocess.Popen(command.split(' '))
    process.wait()
    
main()