#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include <thread>
#include "./TaskQueue/taskqueue.hpp"
using namespace std;
const int TASKS = 4;

bool stop = 0;

struct Tester {
    int id;
    int cnt[TASKS];
    vector<int> rb_config, inter_pt_config, inter_seg_config, bentley_config;
    string rb_command, inter_pt_command, inter_seg_command, bentley_command;

    Tester(){}

    Tester(char **args, int id) {
        this->id = id;
        for(int i=0;i<TASKS;i++) {
            cnt[i] = 0;
        }
        configure(&args[2], 2, rb_config);
        create_command("python3 ../common/Rb.py ", rb_config, 1, rb_command);
        configure(&args[4], 2, inter_pt_config);
        create_command("python3 ../common/GeometricPredicates.py 0 ", inter_pt_config, 1, inter_pt_command);
        configure(&args[6], 2, inter_seg_config);
        create_command("python3 ../common/GeometricPredicates.py 1 ", inter_seg_config, 1, inter_seg_command);
        configure(&args[8], 8, bentley_config);
        create_command("cd ../../.. && python3 -m geocomp.segmentintersection.BentleyOttmanTester ", bentley_config, 1, bentley_command);
    }

    void create_command(string command, vector<int> &args, int ini, string &dest) {
        dest = command;
        for(int i=ini;i<args.size();i++) 
            dest += to_string(args[i]) + " ";
    }

    vector<int> configure(char **v, int n, vector<int> &config) {
        int x;
        for(int i=0;i<n;i++) {
            sscanf(v[i], "%d", &x);
            config.push_back(x);
        }
        return config;
    }

    int rb_test() {
        printf("Thread %d - RB Test %d\n", id, cnt[0]);
        return system(rb_command.c_str());
    }

    int inter_pt_test() {
        printf("Thread %d - INTER_PT Teste %d\n", id, cnt[1]);
        return system(inter_pt_command.c_str());
    }

    int inter_seg_test() {
        printf("Thread %d - INTER_SEG Teste %d\n", id, cnt[2]);
        return system(inter_seg_command.c_str());
    }

    int bentley_test() {
        printf("Thread %d - BENTLEY Teste %d\n", id, cnt[3]);
        return system(bentley_command.c_str());
    }

    void test(int task) {
        int ret;
        cnt[task]++;
        switch (task) {
            case 0:
                ret = rb_test();
                break;
            case 1:
                ret = inter_pt_test();
                break;
            case 2:
                ret = inter_seg_test();
                break;
            case 3:
                ret = bentley_test();
                break;
        }
        if(ret != 0){
            stop = 1;
        }
    }
};

TaskQueue fila(TASKS);
void worker(Tester tester) {
    printf("Thread %d iniciada\n", tester.id);
    int task;
    task = fila.pop();
    while(task != -1 && stop == 0) {
        tester.test(task);
        task = fila.pop();
    }
    if(stop == 1) {
        printf("Thread %d finalizada por erro\n", tester.id);
    } else printf("Thread %d finalizada\n", tester.id);
}

void setup_queue(Tester data) {
    fila.push(0,data.rb_config[0]);
    fila.push(1,data.inter_pt_config[0]);
    fila.push(2,data.inter_seg_config[0]);
    fila.push(3,data.bentley_config[0]);
}

int main(int argc, char *argv[]) {

    const char usage[] = "./tester";
    if(argc < 3) {
        printf("%s\n", usage);
        return 0;
    }
    int THREAD_NUM;

    sscanf(argv[1], "%d", &THREAD_NUM);
    std::thread threads[THREAD_NUM];
    Tester tester[THREAD_NUM];
	for(int i=0;i<THREAD_NUM;i++) {
        tester[i] = Tester(argv, i+1);
    }
    setup_queue(tester[0]);
    
    for(int i=0;i<THREAD_NUM;i++) {
    	threads[i] = std::thread(worker, tester[i]);
	}

	for(int i=0;i<THREAD_NUM;i++) {
		threads[i].join();
	}
}
