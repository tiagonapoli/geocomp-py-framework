#pragma once
#include <mutex>

struct TaskQueue {
    int* tasks;
    int TASK_NUM;
    int sz, turn, op_cnt;
    std::mutex mtx;
    TaskQueue(int TASK_NUM);
    void status();
    int pop();
    void push(int tipo, int qtd);
};

