#include "taskqueue.hpp"

TaskQueue::TaskQueue(int TASK_NUM) {
    this->TASK_NUM = TASK_NUM;
    turn = 0;
    tasks = (int*) malloc(TASK_NUM * sizeof(int));
    for(int i=0;i<TASK_NUM;i++)
        tasks[i] = 0;
}

void TaskQueue::status() {
    printf("\n====Task Queue Status=======\n");
    for(int i=0;i<TASK_NUM;i++) {
        printf("%d ", tasks[i]);
    }
    printf("\nsz: %d  op_cnt: %d\n", sz, op_cnt);
    printf("\n");
}
    
int TaskQueue::pop() {
    int ret;
    mtx.lock();
    //if(op_cnt % 1 == 0) status();  
    if(sz == 0) {
        ret = -1;
    } else {
        ret = turn;
        while(tasks[ret] == 0) ret = (ret+1) % TASK_NUM;
        tasks[ret]--;
        turn = (turn+1) % TASK_NUM;
        sz--;
        op_cnt++;
    }
    if(op_cnt % 10 == 0) status();  
    mtx.unlock();
    return ret;
}

void TaskQueue::push(int tipo, int qtd) {
    mtx.lock();
    sz+=qtd;
    tasks[tipo]+=qtd;
    mtx.unlock();
}
