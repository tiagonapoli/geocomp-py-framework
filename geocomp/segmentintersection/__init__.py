# -*- coding: utf-8 -*-

"""Algoritmos para o Problema da Interseccao de segmentos:

Dado um conjunto de segmentos, determinar todas as interseccoes.

Algoritmos disponiveis:
- Brute Force
- Bentley-Ottman

"""
from . import BentleyOttman
from . import BruteForce

# cada entrada deve ter:
#  [ 'nome-do-modulo', 'nome-da-funcao', 'nome do algoritmo' ]
children = ( 
	( 'BentleyOttman', 'BentleyOttmanRun', 'Bentley-Ottman' ),
	( 'BruteForce', 'BruteForceRun', 'Brute Force' )
)

#children = algorithms

__all__ = [a[0] for a in children]
